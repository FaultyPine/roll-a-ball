using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public float speed = 0.1f;
    public GameObject player;

    public ParticleSystem deathEffect;

    Rigidbody rb;
    
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (player != null) {
            Vector3 dir = Vector3.Normalize(player.transform.position - transform.position);
            rb.AddForce(dir * speed, ForceMode.VelocityChange);
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            ChargeUpAbility charge = other.gameObject.GetComponent<ChargeUpAbility>();
            if (charge != null && charge.isInvin) {
                Destroy(gameObject);
                Instantiate(deathEffect, transform.position, Quaternion.Euler(Vector3.up));
            }
        }
    }
}
