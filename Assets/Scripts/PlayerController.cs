using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public float jumpForce = 300f;

    public TextMeshProUGUI countText;

    public GameObject winText;
    public GameObject loseText;

    ChargeUpAbility chargeUpAbility;

    Rigidbody rb;
    int count;

    float movementX;
    float movementY;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        chargeUpAbility = GetComponent<ChargeUpAbility>();
        count = 0;
        SetCountText();
        winText.SetActive(false);
        loseText.SetActive(false);
    }

    void Update()
    {
        
    }

    void OnJump(InputValue jumpValue) {
        rb.AddForce(new Vector3(0f, jumpForce, 0f));
    }


    void OnMove(InputValue movementValue) {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText() {
        countText.text = "Count: " + count.ToString();
        if (count >= 12) {
            winText.SetActive(true);
        }
    }

    void FixedUpdate() {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * speed);
    }

    Material Dissolve;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("PickUp")) {
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
        }
        else if (other.gameObject.CompareTag("Enemy") && !chargeUpAbility.isInvin) {
            if (Dissolve == null) {
                Dissolve = GetComponent<MeshRenderer>().sharedMaterial;
            }
            StartCoroutine(PlayerDeath());
        }
    }

    IEnumerator PlayerDeath() {
        float dissolveFactor = Dissolve.GetFloat("DissolveFactor");
        while (dissolveFactor < 1.0) {
            dissolveFactor += 0.01f;
            Dissolve.SetFloat("DissolveFactor", dissolveFactor);
            yield return null;
        }
        Destroy(gameObject);
        Time.timeScale /= 2f;
        loseText.SetActive(true);
        Dissolve.SetFloat("DissolveFactor", 0.0f);
    }
}
