using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

public class ChargeUpAbility : MonoBehaviour
{

    public int rushFrames = 5;
    public float rushForce = 1.0f;
    public float rushChargeIncrement = 0.01f;

    public bool isCharging {get; private set;}
    public bool isInvin {get; private set;}
    public ParticleSystem chargeEffect;

    PlayerController playerController;
    Rigidbody rb;
    Vector3 chargeInputDir = Vector3.zero;
    float prevRushForce;

    void Awake()
    {
        playerController = GetComponent<PlayerController>();
        rb = GetComponent<Rigidbody>();
    }
    void Start() {
        isCharging = false;
        isInvin = false;
        chargeEffect.gameObject.SetActive(false);
    }



    void OnMove(InputValue inputValue) {
        Vector2 input = inputValue.Get<Vector2>();
        chargeInputDir = new Vector3 (input.x, 0f, input.y);
    }

    void OnCharge(InputValue inputValue) {
        isCharging = !isCharging;
        rb.isKinematic = isCharging;
        if (isCharging) {
            StartCoroutine(Charging());
        }
    }

    

    IEnumerator Charging() {
        // init chargeup
        isInvin = true;
        prevRushForce = rushForce;
        chargeEffect.transform.position = transform.position;
        chargeEffect.gameObject.SetActive(true);

        // during charge
        while (isCharging) {
            //transform.Rotate(Quaternion.LookRotation(chargeInputDir).eulerAngles);
            //Vector3 c =  /*Quaternion.Euler(0, 90, 0) **/ Vector3.Cross(chargeInputDir, Vector3.up);
            //DrawArrow.ForDebug(transform.position, c);
            //transform.Rotate(c);
            //transform.RotateAround(transform.position, chargeInputDir, 1f);
            rushForce += rushChargeIncrement;
            yield return null; // advance one frame
        }

        // post-chargeup
        StartCoroutine(Rush());
    }

    
    IEnumerator Rush() {
        for (int i = 0; i < rushFrames; i++) {
            rb.AddForce(chargeInputDir * rushForce, ForceMode.Impulse);
            yield return null;
        }
        rushForce = prevRushForce;

        yield return new WaitForSeconds(0.5f);
        isInvin = false;
        chargeEffect.gameObject.SetActive(false);
    }
}
